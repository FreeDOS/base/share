# SHARE

Installs file sharing and locking capabilities on your hard disk - for FreeDOS kernel only

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## SHARE.LSM

<table>
<tr><td>title</td><td>SHARE</td></tr>
<tr><td>version</td><td>08/2006a</td></tr>
<tr><td>entered&nbsp;date</td><td>2007-10-20</td></tr>
<tr><td>description</td><td>File sharing and locking capabilities</td></tr>
<tr><td>summary</td><td>Installs file sharing and locking capabilities on your hard disk - for FreeDOS kernel only</td></tr>
<tr><td>keywords</td><td>share,file sharing</td></tr>
<tr><td>author</td><td>Ron Cemer</td></tr>
<tr><td>maintained&nbsp;by</td><td>Michael Devore (FreeDosStuff -AT- devoresoftware.com) Japheth &lt;mail -at- japheth.de&gt;</td></tr>
<tr><td>primary&nbsp;site</td><td>https://github.com/FDOS/share</td></tr>
<tr><td>platforms</td><td>FreeDOS only</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License](LICENSE)</td></tr>
<tr><td>wiki&nbsp;site</td><td>http://wiki.freedos.org/wiki/index.php/Share</td></tr>
<tr><td>legacy&nbsp;site</td><td>http://www.dosemu.org/~bart/</td></tr>
<tr><td>legacy&nbsp;site</td><td>ftp://ftp.devoresoftware.com/downloads/share.zip</td></tr>
</table>
